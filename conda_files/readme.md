# Installing xcms and CAMERA:

The R packages xcms and CAMERA must be installed through bioconductor.  
To install xcms and CAMERA with conda use:

```console
conda clean --all
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge

# nb: the order; --add gives the requested channel higher priority  
# conda config --show channels
# channels:
#   - conda-forge
#   - bioconda
#   - defaults
conda create --name rchem bioconductor-xcms
conda activate rchem
conda install bioconductor-camera
conda install bioconductor-ipo
```

See the code in setup.R if you prefer to manage R/xcms outside of conda.  

Alternatively, the yml file can be installed simply by opening a bash terminal  
and typing:

```console
conda env create -f rchem_base.yml
```

As a sanity check, confirm that R points to the correct folder:   
```console
which R
R
```

Then, from within R, test whether the libraries are installed:    
```console
require(CAMERA) # xcms is a dep. of camera
```
