import os
import sys
import datetime as dt
import xml.etree.ElementTree as ET
from operator import itemgetter

def get_time_on_machine(xml_files):
    for xml_file in xml_files:
        root = ET.parse(xml_file).getroot()
        ele = root.find('./description/dataProcessing/software')
        yield xml_file, ele.attrib.get("completionTime")


def main(project, files_to_investigate):
    """this function requires environmental variables to be set"""
    main_dir = os.environ.get('METABOLOMIC_PROJECTS')
    sub_dir = os.environ.get('METABODIR_' + project.upper())

    specific_dir = os.path.realpath(os.path.expanduser(main_dir) + sub_dir)
    os.chdir(specific_dir)

    # read the file containing our target xml-files
    with open(files_to_investigate, 'r') as f:
        lines = f.read().splitlines()

    number_of_missing_files = 0
    for name in lines:
        if (not os.path.isfile(name)):
            print(f"File: {name} does not exist.")
            number_of_missing_files += 1

    if (number_of_missing_files > 0):
        print(f"There were {number_of_missing_files} missing files!\nPlease review the input file.")
        return

    # extract and sort the timestamps from each xml-file
    names_and_times = list(get_time_on_machine(lines))
    names_and_times.sort(key=itemgetter(1))

    # write out the filename and timestamps
    output_file = os.path.splitext(files_to_investigate)[0] + '.timestamps.txt'
    with open(output_file, 'w') as f:
        f.write('order\tfilename\ttimestamp\n')
        for index, tuple in enumerate(names_and_times):
            fmtd_time = dt.datetime.strptime(tuple[1], "%Y-%m-%dT%H:%M:%S")
            f.write('{}\t{}\t{}'.format(index, tuple[0], fmtd_time) + '\n')


if __name__ == '__main__':
    if (len(sys.argv) > 1):
        project = sys.argv[1]
        filename = sys.argv[2]
        print("Using project: {} and xml-list (txt file): {}".format(project, filename))
        main(project=project, files_to_investigate=filename)
    else:
        print("Usage: {} project filename".format(sys.modules[__name__]))
