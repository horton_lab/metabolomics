import os
import sys

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap

from itertools import chain

def draw_map(map, scale=0.2):
    # draw the shade-relief image (VanderPlas ML book)
    map.shadedrelief(scale=scale)

    # lat/long as dictionaries:
    lats = map.drawparallels(np.linspace(-90, 90, 13))
    longs = map.drawparallels(np.linspace(-180, 180, 13))

    lats_lines = chain(*(tup[1][0] for tup in lats.items()))
    longs_lines = chain(*(tup[1][0] for tup in longs.items()))
    all_lines = chain(lats_lines, longs_lines)

    # set the desired style for each line
    for line in all_lines:
        line.set(linestyle='-', alpha=0.4, color='w')


def main(project, sample_file, accession_file, lat_offset=8, lon_offset=5):
    """the environmental variables should be set"""
    main_dir = os.environ.get('METABOLOMIC_PROJECTS')
    sub_dir = os.environ.get('METABODIR_' + project.upper())

    specific_dir = os.path.realpath(os.path.expanduser(main_dir) + sub_dir)
    os.chdir(specific_dir)

    ## open, restrict to the characterized data
    accessions = pd.read_csv(accession_file, delimiter='\t')
    samples = pd.read_csv(sample_file, header=None, delimiter='/').iloc[:, 0]
    samples = samples.str.split('_').str[-1] # the accession_id (formerly, ecotype_id)
    subset = accessions[accessions['ecotype_id'].isin(samples.unique().astype(np.int64))]

    ll_lat = min(subset.latitude) - lat_offset
    ur_lat = max(subset.latitude) + lat_offset
    ll_lon = min(subset.longitude) - lon_offset
    ur_lon = max(subset.longitude) + lon_offset # TODO: estimate image size

    fig = plt.figure(figsize=(9, 3), edgecolor='grey')
    m = Basemap(projection='cyl', resolution=None,
                llcrnrlat=ll_lat, urcrnrlat=ur_lat,
                llcrnrlon=ll_lon, urcrnrlon=ur_lon)
    draw_map(map=m)
    m.scatter(subset.longitude, subset.latitude, latlon=True, cmap='Blues')

    pdf_name = os.path.splitext(sample_file)[0] + '.map.pdf'
    plt.savefig(pdf_name)

if __name__ == '__main__':
    if (len(sys.argv) > 2):
        project = sys.argv[1]
        sample_list = sys.argv[2]
        accn_list = sys.argv[3]

        if sample_list == 'def':
            sample_list = 'lines.toalign.gte3.txt'

        if accn_list == 'def':
            accn_list = 'all_ecotypes.txt'

        print("For the {} project, the user requested sample file: {}\n and accession file: {}".format(project, sample_list, accn_list))
        main(project=project, sample_file=sample_list, accession_file=accn_list)

    else:
        print("Usage: {} project_name sample_file accession_file".format(sys.modules[__name__]))
