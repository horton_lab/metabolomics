# This R script plots the distribution of TICs
#
# Author: mhorto
###############################################################################

rm(list = ls());

suppressMessages(library(CAMERA, quietly=TRUE));
library(ggplot2); library(scales);

source(normalizePath(file.path(Sys.getenv('METABOLOMIC_CODE'), 'hdr.metabolomic_methods.R')));

###############################################################################
## user variables
###############################################################################
organ <- "roots";
blanks <- FALSE;
prefilter <- c(3, 1000);

outputDirectory <- "plots/qc/offsets/";
showMean <- TRUE;
histColor <- "burlywood";

{
	setwd(file.path(getProjectDirectory(organ, prefilter = prefilter)));

	if (organ == "roots") {
		if (blanks) {
			covariateFilters <- list(numberOfPlants = list(range = c(0, 0)));

		} else {
			covariateFilters <- list(numberOfPlants = list(range = c(1, Inf)));
		}

	} else if (organ == "leaves") {
		covariateFilters <- list(flowering_status = list(keep = ""));
	}

	if( !dir.exists(outputDirectory)){
		dir.create(outputDirectory, recursive=TRUE);
		cat("Created output directory:", outputDirectory, "\n");
	};

	###############################################################################
	## open the robject
	###############################################################################
	cat("\nPlease choose the metabolomics robject.\n");
	dataset <- openMetabolomicDataset(organ = organ, annotated = TRUE, blanks = blanks);
	covariates <- dataset$covariates;
	baseFileName <- gsub("Robj", "", basename(dataset$filename));

	###############################################################################
	## tally the mean retention time and # of ions, for each feature
	###############################################################################
	peaks <- getPeakMatrix(dataset$obj);
	pcgroupInfo <- peaks$covariates;
	peaks <- peaks$peaks;

	###############################################################################
	## optional: filter by sample-covariate (e.g. flowering or not)
	###############################################################################
	if (!is.null(covariateFilters)) {
		subsetted <- subsetPeakByCovariateMatrix(peaks, covariates, factorsToSubset = covariateFilters);
		peaks <- subsetted$newPeakMatrix;
		pcgroupInfo <- pcgroupInfo[rownames(peaks),];
		covariates <- subsetted$newCovariates;
	}

	###############################################################################
	## estimate the total ion count (TIC)
	###############################################################################
	offsets <- mstack(colSums(peaks), sorted = F, newHeaders = c("match_id", "tic"));
	offsets <- merge(offsets, covariates, by = "match_id");

	###############################################################################
	## plot the distribution of maximum peaks by rt
	###############################################################################
	gg <- ggplot(offsets) +
				theme_bw(base_size=16) +
				theme(axis.text.x=element_text(size=9, angle=25, vjust=1, hjust=1),
					  axis.text.y=element_text(size=11)) +
				geom_histogram(aes(tic), fill=histColor) +
				xlab("Total ion count (TIC)") +
				ylab("Number of samples");

	# plot the mean
	if( showMean ){
		meanTic <- mean(offsets[,"tic"]);
		gg <- gg +
					geom_vline(xintercept=meanTic, linetype=3, lwd=1, colour="grey") +
					scale_y_continuous(expand = c(0, 0));
	}

	pdfOutputFileName <- file.path(outputDirectory, paste0("distn_tics.", baseFileName, "pdf"));
	ggsave(filename=pdfOutputFileName, gg, device="pdf", scale=1, height=3, width=4.5, useDingbats=FALSE);
}
