### Analyzing metabolomic data

Authors: Matt Horton

This package contains scripts (in _R_ and Python) for analyzing metabolomics data.  
The current version is 1.0.  

Suggestions and code contributions are welcome.

### The main dependencies are:
* Python 3+ 
    - NumPy, Pandas, sklearn, ...
* R
    - xcms related (available through bioconductor/bioconda): IPO, xcms, CAMERA, ...,
    - lme4
    - vegan
    - pheatmap
  
* ...

### Setting the environment

The R scripts use _Sys.getenv()_ to avoid hardcoded paths.  

You will need to modify the variables within .Renviron (main folder) to fit your environment.  
*conda users: the yaml file and environmental variable scripts are under the conda_files directory.

Thanks,  
Matt
